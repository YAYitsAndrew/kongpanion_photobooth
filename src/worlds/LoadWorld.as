package worlds
{

import com.greensock.events.LoaderEvent;
import com.greensock.loading.ImageLoader;
import com.greensock.loading.LoaderMax;

import net.flashpunk.Entity;
import net.flashpunk.World;
import net.flashpunk.graphics.Text;

public class LoadWorld extends World
{
	protected var _totalHeight : Number;
	protected var _messages : Vector.<Text>;
	private var _messageEntities : Vector.<Entity>;
	
	override public function begin() : void
	{
		super.begin();
		
		_messages = new <Text>[];
		_messageEntities = new <Entity>[];
	}
	
	protected function loadKongpanions() : void
	{
		loadKongpanionIcons();
	}
	
	private function loadKongpanionIcons() : void
	{
		var queue : LoaderMax = new LoaderMax( { onComplete : onKongpanionIconsLoaded,
			onProgress: onKongpanionIconsProgress } );
		for ( var i : int = 0; i < KongpanionPhotobooth.instance.kongpanionDatas.length; i++ )
		{
			var kd : KongpanionData = KongpanionPhotobooth.instance.kongpanionDatas[ i ];
			queue.append( new ImageLoader( kd.normalIconUrl, { name : kd.name } ) );
			queue.append( new ImageLoader( kd.shinyIconUrl, { name : kd.name + "_shiny" } ) );
		}
		queue.load();
		
		addMessage( "Loading Kongpanion artwork...  0%" );
	}
	
	private function onKongpanionIconsProgress( a_event : LoaderEvent ) : void
	{
		var lm : LoaderMax = a_event.target as LoaderMax;
		
		var percentStr : String = int( lm.progress * 100 ).toString();
		if( lm.progress < 0.1 )
		{
			percentStr = " " + percentStr;
		}
		
		var msg : Text = _messages[ _messages.length - 1 ];
		msg.text = msg.text.substr( 0, msg.text.length - 3 ) + percentStr + "%";
	}
	
	private function onKongpanionIconsLoaded( a_event : LoaderEvent ) : void
	{
		onKongpanionLoadComplete();
	}
	
	protected function onKongpanionLoadComplete() : void
	{
		trace( "LoadWorld.onKongpanionLoadComplete: override me!" );
	}
	
	protected function addMessage( a_msg : String ) : void
	{
		var msg : Text = new Text( a_msg, 0, 0, { size : 16 } );
		msg.x = 8;
		msg.y = _totalHeight;
		
		var e : Entity = this.addGraphic( msg );
		_messageEntities.push( e );
		
		_messages.push( msg );
		_totalHeight += msg.height;
	}
	
	protected function finishLastMessage() : void
	{
		var msg : Text = _messages[ _messages.length - 1 ];
		msg.text += "done.";
	}
	
	protected function clearMessages() : void
	{
		for( var i : int = 0; i < _messageEntities.length; i++ )
		{
			this.remove( _messageEntities[ i ] );
		}
		
		_messageEntities.length = 0;
		_messages.length = 0;
	}
}

}