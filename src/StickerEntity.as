package
{

import com.greensock.loading.LoaderMax;
import com.greensock.loading.display.ContentDisplay;

import flash.display.Bitmap;
import flash.display.BitmapData;

import net.flashpunk.graphics.Image;

import worlds.PhotoboothWorld;

public class StickerEntity extends OffsetDraggableEntity
{
	private var _img : Image;
	private var _data : KongpanionData;
	private var _isShiny : Boolean;
	
	public function StickerEntity( a_data : KongpanionData, a_isShiny : Boolean )
	{
		super();
		
		this.type = "interactive";
		this.layer = 10;
		this.active = false;
		
		var bmpName : String = a_data.name;
		if( a_isShiny )
		{
			bmpName += "_shiny";
		}
		
		_data = a_data;
		_isShiny = a_isShiny;
		
		var content : ContentDisplay = LoaderMax.getContent( bmpName );
		var bd : BitmapData = ( content.rawContent as Bitmap ).bitmapData;
		content = null;
		
		_img = new Image( Assets.trimWhitespace( bd ) );
		_img.centerOrigin();
		_img.x = _img.width / 2;
		_img.y = _img.height / 2;
		_img.smooth = true;
		
		this.setHitbox( _img.width, _img.height );
		this.addGraphic( _img );
	}
	
	override public function update() : void
	{
		super.update();
		
		var photo : Image = ( this.world as PhotoboothWorld ).photo;
		this.clampHorizontal( photo.x, photo.x + photo.scaledWidth,
			- Math.abs(_img.scaledWidth ) * 0.75 );
		this.clampVertical( photo.y, photo.y + photo.scaledHeight, - _img.scaledHeight * 0.75 );
	}
	
	override public function onStartDrag() : void
	{
		super.onStartDrag();
		
		( this.world as PhotoboothWorld ).setActiveSticker( this );
	}
	
	override public function onEndDrag() : void
	{
		super.onEndDrag();
		
		this.active = false;
	}
	
	public function mirror() : void
	{
		_img.scaleX *= -1;
	}
	
	public function get scale() : Number { return _img.scale; }
	public function set scale( a_val : Number ) : void
	{
		_img.scale = a_val;
		updatePositionAndHitbox();
	}
	
	public function get rotation() : Number { return _img.angle * ( Math.PI / 180 ); }
	public function set rotation( a_val : Number ) : void
	{
		_img.angle = a_val * ( 180 / Math.PI );
		updatePositionAndHitbox();
	}
	
	private function updatePositionAndHitbox() : void
	{
		_img.x = Math.abs( _img.scaledWidth ) / 2;
		_img.y = _img.scaledHeight / 2;
		
		this.setHitbox( Math.abs( _img.scaledWidth ), _img.scaledHeight );
	}
	
	public function get isMirrored() : Boolean { return _img.scaleX < 0; }
	public function get data() : KongpanionData { return _data; }
	public function get isShiny() : Boolean { return _isShiny; }
}

}