package worlds
{

import com.adobe.images.PNGEncoder;

import flash.display.BitmapData;
import flash.events.MouseEvent;
import flash.geom.Matrix;
import flash.geom.Rectangle;
import flash.net.FileReference;
import flash.ui.MouseCursor;
import flash.utils.ByteArray;

import net.flashpunk.Entity;
import net.flashpunk.FP;
import net.flashpunk.World;
import net.flashpunk.graphics.Image;
import net.flashpunk.utils.Input;

import ui.ActiveButton;

public class PhotoboothWorld extends World
{
	private var _photoData : BitmapData;
	private var _photo : Image;
	private var _stickerButton : ActiveButton;
	private var _deleteButton : ActiveButton;
	private var _cloneButton : ActiveButton;
	private var _backButton : ActiveButton;
	private var _saveButton : ActiveButton;
	private var _stickerDrawer : Stickerbook;
	private var _stickers : Vector.<StickerEntity>;
	private var _activeSticker : StickerEntity;
	private var _resize : ResizeEntity;
	private var _rotate : RotateEntity;
	private var _mirror : MirrorEntity;
	private var _isBackClicked : Boolean;
	
	public function PhotoboothWorld( a_photoData : BitmapData )
	{
		super();
		
		_photoData = a_photoData;
		_isBackClicked = false;
	}
	
	override public function begin() : void
	{
		super.begin();
		
		_photo = new Image( _photoData );
		_photo.smooth = true;
		this.addGraphic( _photo ).layer = 100;
		scalePhotoToScreen();
		
		var botMatte : Image = Image.createRect( 100, 100, 0x9FB6CD );
		botMatte.y = _photo.scaledHeight;
		botMatte.scaleX = FP.screen.width / botMatte.width;
		botMatte.scaleY = ( FP.screen.height - _photo.scaledHeight ) / botMatte.width;
		this.addGraphic( botMatte );
		
		var leftMatte : Image = Image.createRect( 100, 100, 0x9FB6CD );
		leftMatte.scaleX = _photo.x / leftMatte.width;
		leftMatte.scaleY = FP.screen.height / leftMatte.width;
		this.addGraphic( leftMatte );
		
		var rightMatte : Image = Image.createRect( 100, 100, 0x9FB6CD );
		rightMatte.x = _photo.x + _photo.scaledWidth;
		rightMatte.scaleX = ( FP.screen.width - _photo.x ) / rightMatte.width;
		rightMatte.scaleY = FP.screen.height / rightMatte.width;
		this.addGraphic( rightMatte );
		
		_stickers = new <StickerEntity>[];
		loadStickerDrawer();
		
		FP.stage.addEventListener( MouseEvent.CLICK, onMouseClick );
	}
	
	private function onMouseClick( a_event : MouseEvent ) : void
	{
		if( ! _saveButton.active ) return;
		
		if( _saveButton.collidePoint( _saveButton.x, _saveButton.y, a_event.stageX, a_event.stageY ) )
		{
			onSaveClick();
		}
	}
	
	private function loadStickerDrawer() : void
	{
		var s : StickerbookEntity;
		var stickerbookEntities : Vector.<StickerbookEntity> = new <StickerbookEntity>[];
		for ( var i : int = 0; i < KongpanionPhotobooth.instance.kongpanionDatas.length; i++ )
		{
			s = new StickerbookEntity( KongpanionPhotobooth.instance.kongpanionDatas[ i ], false );
			stickerbookEntities.push( s );
			
			s = new StickerbookEntity( KongpanionPhotobooth.instance.kongpanionDatas[ i ], true );
			stickerbookEntities.push( s );
		}
		
		_backButton = new ActiveButton( 0, 0, "< Back", 150, 50, onBackClick );
		_backButton.x = 8;
		_backButton.y = FP.screen.height - _backButton.height - 4;
		this.add( _backButton );
		
		_stickerButton = new ActiveButton( 0, 0, "Kongpanions", 150, 80, onStickerClick );
		_stickerButton.x = ( FP.screen.width - _stickerButton.width ) / 2;
		_stickerButton.y = FP.screen.height - _stickerButton.height - 4;
		this.add( _stickerButton );
		
		_deleteButton = new ActiveButton( 0, 0, "Delete", 70, 50, onDeleteClick );
		_deleteButton.x = _stickerButton.x - _deleteButton.width - 8;
		_deleteButton.y = FP.screen.height - _deleteButton.height - 4;
		this.add( _deleteButton );
		
		_cloneButton = new ActiveButton( 0, 0, "Clone", 70, 50, onCloneClick );
		_cloneButton.x = _stickerButton.x + _stickerButton.width + 8;
		_cloneButton.y = _deleteButton.y;
		this.add( _cloneButton );
		
		_saveButton = new ActiveButton( 0, 0, "Save >", 150, 50 );
		_saveButton.x = FP.screen.width - _saveButton.width - 8;
		_saveButton.y = _backButton.y;
		this.add( _saveButton );
		
		_stickerDrawer = new Stickerbook( stickerbookEntities, this );
		_stickerDrawer.hide();
	}
	
	private function onBackClick() : void
	{
		if( _isBackClicked )
		{
			FP.stage.removeEventListener( MouseEvent.CLICK, onMouseClick );
			
			FP.world = new ChooseSourceWorld();
		}
		else
		{
			_isBackClicked = true;
			_backButton.changeText( "Click to confirm" );
		}
	}
	
	private function onSaveClick() : void
	{
		setActiveSticker( null );
		this.render();
		
		var bd : BitmapData = new BitmapData( _photo.scaledWidth, _photo.scaledHeight );
		var rect : Rectangle = new Rectangle( 0, 0, bd.width, bd.height );
		
		var mat : Matrix = new Matrix();
		var trans : Matrix = new Matrix();
		trans.translate( - _photo.x, - _photo.y );
		mat.concat( trans );
		
		bd.draw( FP.stage, mat, null, null, rect );
		
		var ba : ByteArray = PNGEncoder.encode( bd );
		
		var fr : FileReference = new FileReference();
		fr.save( ba, "kongpanion_photobooth.png.png" );
	}
	
	override public function update() : void
	{
		if( Input.mousePressed )
		{
			var top : Entity = topmostCollidePoint( "interactive", Input.mouseX, Input.mouseY );
			if( top != null )
			{
				if( top is StickerEntity )
				{
					top.active = true;
				}
			}
			else
			{
				setActiveSticker( null );
			}
		}
		
		Input.mouseCursor = MouseCursor.AUTO;
		
		super.update();
		
		if( Input.mousePressed && _isBackClicked
			&& ! _backButton.collidePoint( _backButton.x, _backButton.y, Input.mouseX,
				Input.mouseY ) )
		{
			_isBackClicked = false;
			_backButton.changeText( "< Back" );
		}
	}
	
	private function onStickerClick() : void
	{
		_deleteButton.active = false;
		_stickerButton.active = false;
		_cloneButton.active = false;
		_backButton.active = false;
		_saveButton.active = false;
		_stickerDrawer.show();
	}
	
	private function onDeleteClick() : void
	{
		if( _activeSticker == null ) return;
		
		var cacheSticker : StickerEntity = _activeSticker;
		setActiveSticker( null );
		
		this.remove( cacheSticker );
		_stickers.splice( _stickers.indexOf( cacheSticker ), 1 );
	}
	
	private function onCloneClick() : void
	{
		if( _activeSticker == null) return;
		
		var rot : Number = _activeSticker.rotation;
		var sc : Number = _activeSticker.scale;
		var mirror : Boolean = _activeSticker.isMirrored;
		
		var s : StickerEntity = createSticker( _activeSticker.data, _activeSticker.isShiny );
		s.rotation = rot;
		s.scale = sc;
		if( mirror )
		{
			s.mirror();
		}
		
		setActiveSticker( s );
	}
	
	private function scalePhotoToScreen() : void
	{
		var widthScale : Number = FP.screen.width / _photo.width;
		var heightScale : Number = ( FP.screen.height - 90 ) / _photo.height;
		if( widthScale < heightScale )
		{
			_photo.scale = widthScale;
		}
		else
		{
			_photo.scale = heightScale;
		}
		
		_photo.x = ( FP.screen.width - _photo.scaledWidth ) / 2;
	}
	
	public function createSticker( a_data : KongpanionData, a_isShiny : Boolean ) : StickerEntity
	{
		var s : StickerEntity = new StickerEntity( a_data, a_isShiny );
		s.x = FP.screen.width / 2;
		s.y = FP.screen.height / 2;
		this.add( s );
		
		_stickers.push( s );
		
		closeStickerDrawer();
		
		return s;
	}
	
	public function closeStickerDrawer() : void
	{
		_stickerDrawer.hide();
		_stickerButton.active = true;
		_deleteButton.active = true;
		_cloneButton.active = true;
		_backButton.active = true;
		_saveButton.active = true;
	}
	
	public function setActiveSticker( a_sticker : StickerEntity ) : void
	{
		if( _activeSticker == a_sticker ) return;
		
		if( _activeSticker != null )
		{
			this.remove( _resize );
			_resize.visible = false;
			_resize = null;
			this.remove( _rotate );
			_rotate.visible = false;
			_rotate = null;
			this.remove( _mirror );
			_mirror.visible = false;
			_mirror = null;
		}
		
		_activeSticker = a_sticker;
		
		if( _activeSticker != null )
		{
			this.bringToFront( _activeSticker );
			
			_resize = new ResizeEntity( _activeSticker );
			this.add( _resize );
			
			_rotate = new RotateEntity( _activeSticker );
			this.add( _rotate );
			
			_mirror = new MirrorEntity( _activeSticker );
			this.add( _mirror );
		}
	}
	
	public function topmostCollidePoint(type:String, pX:Number, pY:Number):Entity 
	{
		// Create storage
		var collidingEntities:Vector.<Entity> = new Vector.<Entity>;
		
		// Gather units
		FP.world.collidePointInto(type, pX, pY, collidingEntities);
		
		// Check for collisions
		if (collidingEntities.length > 0)
		{
			// Sort the vector numerically by layer
			FP.sortBy(collidingEntities, "layer");
			
			// Get entities on colliding layer
			var layerEntities:Vector.<Entity> = new Vector.<Entity>;
			FP.world.getLayer(collidingEntities[0].layer, layerEntities);
			
			// Get topmost entity
			var i:int = layerEntities.length - 1;
			while (i > 0 && collidingEntities.indexOf(layerEntities[i]) < 0) i--;
			
			return layerEntities[i];
		}
		
		return null;
	}
	
	public function get photo() : Image { return _photo; }
}

}