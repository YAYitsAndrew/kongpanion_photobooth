package worlds
{

import com.greensock.events.LoaderEvent;
import com.greensock.loading.ImageLoader;
import com.greensock.loading.LoaderMax;
import com.greensock.loading.display.ContentDisplay;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.geom.Rectangle;
import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;
import flash.ui.MouseCursor;

import net.flashpunk.FP;
import net.flashpunk.graphics.Canvas;
import net.flashpunk.graphics.Text;
import net.flashpunk.utils.Input;

import ui.ActiveButton;
import ui.ActiveTextInput;

public class LoadFromUrlWorld extends LoadWorld
{
	private var _copyPaste : TextField;
	private var _input : ActiveTextInput;
	private var _loadButton : ActiveButton;
	private var _photoData : BitmapData;
	
	override public function begin() : void
	{
		super.begin();
		
		var msg : Text = new Text( "Paste a URL", 0, 0, { size : 16 } );
		msg.x = 8;
		msg.y = 16;
		
		var bg : Canvas = new Canvas( FP.screen.width, msg.height + 32 );
		bg.fill( new Rectangle( 0, 0, bg.width, bg.height ), 0x181818 );
		bg.alpha = 0.7;
		bg.y = msg.y - 16;
		this.addGraphic( bg );
		
		this.addGraphic( msg );
		
		_loadButton = new ActiveButton( 0, 0, "Load", 150, 50, onLoadClick );
		_loadButton.x = FP.screen.width - _loadButton.width - 8;
		_loadButton.y = bg.y + bg.height + 8;
		this.add( _loadButton );
		
		_input = new ActiveTextInput( 0, 0, "", false, FP.screen.width - _loadButton.width - 24 );
		_input.x = 8;
		_input.y = _loadButton.y + ( _loadButton.height - _input.height ) / 2;
		this.add( _input );
		_input.active = false;
		
		addPasteCollector();
		
		_totalHeight = _loadButton.y + _loadButton.height + 8;
		
		var backButton : ActiveButton = new ActiveButton( 0, 0, "Back", 150, 50, onBackClick );
		backButton.x = 8;
		backButton.y = FP.screen.height - backButton.height - 8;
		this.add( backButton );
	}
	
	private function onBackClick() : void
	{
		removePasteCollector();
		
		FP.world = new ChooseSourceWorld();
	}
	
	private function onLoadClick() : void
	{
		_loadButton.active = false;
		
		var queue : LoaderMax = new LoaderMax( { onComplete : onPhotoLoaded,
			onError : onPhotoError } );
		queue.skipFailed = false;
		queue.append( new ImageLoader( _copyPaste.text, { name : "photo" } ) );
		queue.load();
		
		addMessage( "Loading image..." );
	}
	
	private function onPhotoLoaded( a_event : LoaderEvent ) : void
	{
		finishLastMessage();
		
		var content : ContentDisplay = LoaderMax.getContent( "photo" );
		_photoData = ( content.rawContent as Bitmap ).bitmapData;
		
		loadKongpanions();
	}
	
	override protected function onKongpanionLoadComplete() : void
	{
		removePasteCollector();
		
		FP.world = new PhotoboothWorld( _photoData );
	}
	
	private function onPhotoError( a_event : LoaderEvent ) : void
	{
		addMessage( "Error loading: " + a_event.text );
		
		_loadButton.active = true;
	}
	
	override public function update() : void
	{
		Input.mouseCursor = MouseCursor.AUTO;
		
		super.update();
	}
	
	private function addPasteCollector() : void
	{
		_copyPaste = new TextField();
		_copyPaste.wordWrap = false;
		_copyPaste.multiline = false;
		_copyPaste.defaultTextFormat = new TextFormat( null, 20 );
		_copyPaste.type = TextFieldType.INPUT;
		_copyPaste.width = _input.width;
		_copyPaste.height = _input.height;
		_copyPaste.x = _input.x;
		_copyPaste.y = _input.y;
		FP.stage.addChild( _copyPaste );
		
		FP.stage.focus = _copyPaste;
	}
	
	private function removePasteCollector() : void
	{
		FP.stage.removeChild( _copyPaste );
	}
}

}