package
{

import flash.geom.Rectangle;
import flash.net.URLRequest;
import flash.net.navigateToURL;

import net.flashpunk.Entity;
import net.flashpunk.FP;
import net.flashpunk.graphics.Canvas;

import ui.ActiveButton;

import worlds.PhotoboothWorld;

public class Stickerbook
{
	private var _world : PhotoboothWorld;
	private var _bg : Canvas;
	private var _left : ActiveButton;
	private var _right : ActiveButton;
	private var _close : ActiveButton;
	private var _earn : ActiveButton;
	private var _entities : Vector.<StickerbookEntity>;
	private var _page : int;
	private var _numPages : int;
	
	public function Stickerbook( a_entities : Vector.<StickerbookEntity>, a_world : PhotoboothWorld )
	{
		_entities = a_entities;
		_world = a_world;
		_page = 0;
		_numPages = 1;
		
		_bg = new Canvas( FP.screen.width, 90 );
		_bg.fill( new Rectangle( 0, 0, _bg.width, _bg.height ), 0xCCCCCC );
		_bg.y = FP.screen.height - _bg.height;
		_world.addGraphic( _bg );
		
		for( var i : int = 0; i < _entities.length; i++ )
		{
			_world.add( _entities[ i ] );
			_entities[ i ].y = _bg.y + ( _bg.height - _entities[ i ].height ) / 2;
		}
		
		_left = new ActiveButton( 0, 0, "<", 32, _bg.height - 6, onLeftClick );
		_left.x = _bg.x + 3;
		_left.y = _bg.y + 3;
		_world.add( _left );
		
		_right = new ActiveButton( 0, 0, ">", _left.width, _left.height, onRightClick );
		_right.x = _bg.x + _bg.width - _right.width - 3;
		_right.y = _left.y;
		_world.add( _right );
		
		_close = new ActiveButton( 0, 0, "Close", 60, 30, onCloseClick );
		_close.x = FP.screen.width - _close.width - 3;
		_close.y = _bg.y - _close.height;
		_world.add( _close );
		
		_earn = new ActiveButton( 0, 0, "Earn more", 90, _close.height, onEarnClick );
		_earn.x = _close.x - _earn.width - 6;
		_earn.y = _close.y;
		_world.add( _earn );
		
		updatePositions();
	}
	
	public function show() : void
	{
		toggleVisibility( true );
	}
	
	public function hide() : void
	{
		toggleVisibility( false );
	}
	
	private function toggleVisibility( a_val : Boolean ) : void
	{
		_bg.visible = a_val;
		_left.visible = a_val;
		_right.visible = a_val;
		_close.visible = a_val;
		_earn.visible = a_val;
		
		_left.active = a_val;
		_right.active = a_val;
		_close.active = a_val;
		_earn.active = a_val;
		
		for( var i : int = 0; i < _entities.length; i++ )
		{
			_entities[ i ].visible = a_val;
		}
		
		if( a_val )
		{
			updatePositions();
		}
	}
	
	private function updatePositions() : void
	{
		var padding : Number = ( _left.x + _left.width + 3 );
		var pageWidth : Number = FP.screen.width - padding * 2;
		
		var runningTotal : Number = padding;
		var runningPage : int = 0;
		for( var i : int = 0; i < _entities.length; i++ )
		{
			var e : Entity = _entities[ i ];
			var runningAddtl : Number = e.width + 8;
			
			if( runningTotal + runningAddtl > pageWidth )
			{
				runningTotal = padding;
				runningPage += 1;
			}
			
			e.x = runningTotal;
			runningTotal += runningAddtl;
			
			e.visible = ( runningPage == _page );
		}
		
		_numPages = runningPage + 1;
	}
	
	private function onLeftClick() : void
	{
		_page -= 1;
		if( _page < 0 )
		{
			_page = 0;
		}
		
		updatePositions();
	}
	
	private function onRightClick() : void
	{
		_page += 1;
		if( _page >= _numPages )
		{
			_page = _numPages - 1;
		}
		
		updatePositions();
	}
	
	private function onCloseClick() : void
	{
		_world.closeStickerDrawer();
	}
	
	private function onEarnClick() : void
	{
		var request : URLRequest = new URLRequest( "http://www.kongregate.com/kongpanions" );
		navigateToURL( request, "_blank" );
	}
}

}