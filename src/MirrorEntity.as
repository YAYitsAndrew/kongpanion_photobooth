package
{

import net.flashpunk.Entity;
import net.flashpunk.graphics.Image;
import net.flashpunk.utils.Input;

public class MirrorEntity extends Entity
{
	private var _img : Image;
	private var _sticker : StickerEntity;
	
	public function MirrorEntity( a_sticker : StickerEntity )
	{
		super();
		
		this.type = "interactive";
		
		_sticker = a_sticker;
		
		_img = new Image( Assets.MIRROR_HANDLE );
		this.setHitboxTo( _img );
		this.addGraphic( _img );
	}
	
	override public function update() : void
	{
		super.update();
		
		if( Input.mouseReleased && this.collidePoint( x, y, Input.mouseX, Input.mouseY ) )
		{	
			_sticker.mirror();
		}
		
		this.x = _sticker.x + _sticker.width;
		this.y = _sticker.y + _sticker.height - this.height - 2;
	}
}

}