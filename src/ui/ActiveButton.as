package ui
{
	import flash.filters.GlowFilter;
	import flash.geom.Matrix;
	
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.graphics.Text;
	
	/**
	 * ...
	 * @author Abel Toy (Rolpege) [http://abeltoy.com/]
	 */
	public class ActiveButton extends Button 
	{
		protected var normal:Stamp;
		protected var hover:Stamp;
		
		protected var label:Text;
		
		public function ActiveButton(x:Number=0, y:Number=0, text:String="", width:Number=150, height:Number=50, callback:Function=null, params:Object=null) 
		{
			super(x, y, text, width, height, callback, params);
			
			this.type = "interactive";
			
			//store the stamp for each state
			normal = drawButton(width, height, 0xD85D09, 0xB13300, 0xDE700C, 0xC13D00);
			hover = drawButton(width, height, 0x5A5047, 0x302920, 0x776D5D, 0x3D3329);
			
			drawLabel();
			
			graphic = normal;
		}
		
		protected function drawButton(width:Number, height:Number, topColor:uint, bottomColor:uint, borderTop:uint, borderBottom:uint):Stamp
		{
			//draws two boxes. one, 1px larger than the other, is the border, with a gradient. The other is the filler, with another gradient
			var gradientMatrix:Matrix = new Matrix();
			gradientMatrix.createGradientBox(width, height, 270 * FP.RAD);
			
			g.beginGradientFill("linear", [borderTop, borderBottom], [1, 1], [0, 255], gradientMatrix);
			g.drawRect(0, 0, width, height);
			g.endFill();
			
			gradientMatrix.createGradientBox(width - 2, height - 2, 270 * FP.RAD, 1, 1);
			g.beginGradientFill("linear", [topColor, bottomColor], [1, 1], [0, 255], gradientMatrix);
			g.drawRect(1, 1, width - 2, height - 2);
			g.endFill();
			
			//outline
			sprite.filters = [new GlowFilter(0x000000, 1, 2, 2, 6, 2)];
			
			return drawStamp(width + 2, height + 2, 1, 1);
		}
		
		public function changeText( a_val : String ) : void
		{
			this.text = a_val;
			drawLabel();
		}
		
		protected function drawLabel():void
		{
			//makes the label
			var t : Text = new Text( text );
			t.align = "center";
			t.width = width;
			t.height = height;
			
			label = t;
			label.y = (height - t.textHeight) / 2;
		}
		
		override protected function changeState(state:int = 0):void 
		{
			if (state == lastState) return;
			
			switch(state)
			{
				//normal and down will be the same image
				case NORMAL:
				case DOWN:
					graphic = normal;
					break;
				case HOVER:
					graphic = hover;
					break;
			}
			
			super.changeState(state);
		}
		
		override public function render():void 
		{
			super.render();
			
			renderGraphic(label);
		}
	}

}