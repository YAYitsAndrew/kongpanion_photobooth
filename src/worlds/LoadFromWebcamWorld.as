package worlds
{

import flash.display.BitmapData;
import flash.events.MouseEvent;
import flash.events.StatusEvent;
import flash.media.Camera;
import flash.media.Video;
import flash.system.Security;
import flash.system.SecurityPanel;
import flash.ui.MouseCursor;

import net.flashpunk.FP;
import net.flashpunk.graphics.Text;
import net.flashpunk.utils.Input;

import ui.ActiveButton;

public class LoadFromWebcamWorld extends LoadWorld
{
	private var _camera : Camera;
	private var _video : Video;
	private var _photoData : BitmapData;
	private var _takePhotoButton : ActiveButton;
	
	override public function begin() : void
	{
		super.begin();
		
		_totalHeight = 8;
		
		var backButton : ActiveButton = new ActiveButton( 0, 0, "Back", 150, 50, onBackClick );
		backButton.x = 8;
		backButton.y = FP.screen.height - backButton.height - 8;
		this.add( backButton );
		
		_takePhotoButton = new ActiveButton( 0, 0, "Take photo", 200, 50, onTakePhotoClick );
		_takePhotoButton.x = ( FP.screen.width - _takePhotoButton.width ) / 2;
		_takePhotoButton.y = backButton.y;
		this.add( _takePhotoButton );
		
		tryAddingCamera();
	}
	
	private function tryAddingCamera() : Boolean
	{
		_takePhotoButton.active = false;
		_takePhotoButton.visible = false;
		
		if( Camera.names.length == 0 )
		{
			addMessage( "No webcam detected." );
			return false;
		}
		
		_camera = Camera.getCamera(); 
		if( _camera == null )
		{
			addMessage( "No webcam detected." );
			return false;
		}
		
		if( checkCameraMuted() )
		{
			return false;
		}
		
		clearMessages();
		
		_camera.addEventListener( StatusEvent.STATUS, onCameraStatus );
		_camera.setMode( 640, 480, 30 );
		_camera.setQuality( 0, 100 );
		
		_video = new Video( _camera.width, _camera.height ); 
		_video.attachCamera( _camera );
		_video.smoothing = true;
		FP.stage.addChild( _video );
		
		var scaleWidth : Number = FP.screen.width / _camera.width;
		var scaleHeight : Number = ( FP.screen.height - 90 ) / _camera.height;
		var scale : Number = ( scaleWidth > scaleHeight ) ? scaleHeight : scaleWidth;
		
		_video.width *= scale;
		_video.height *= scale;
		_video.x = ( FP.screen.width - _video.width ) / 2;
		
		_takePhotoButton.active = true;
		_takePhotoButton.visible = true;
		
		return true;
	}
	
	private function onTakePhotoClick() : void
	{
		_takePhotoButton.active = false;
		
		addMessage( "Photo taken!" );
		
		_photoData = new BitmapData( _camera.width, _camera.height );
		_camera.drawToBitmapData( _photoData );
		
		removeCamera();
		
		loadKongpanions();
	}
	
	override protected function onKongpanionLoadComplete() : void
	{
		FP.world = new PhotoboothWorld( _photoData );
	}
	
	private function onCameraStatus( a_event : StatusEvent ) : void
	{
		_camera.removeEventListener( StatusEvent.STATUS, onCameraStatus );
		
		checkCameraMuted();
	}
	
	private function checkCameraMuted() : Boolean
	{
		if( _camera.muted )
		{
			addMessage( "User denied webcam access." );
			var lastMsg : Text = _messages[ _messages.length - 1 ];
			
			var settingsButton : ActiveButton = new ActiveButton( 0, 0, "Allow", 75, 25,
				onSettingsClick );
			settingsButton.x = lastMsg.x + lastMsg.width + 8;
			settingsButton.y = lastMsg.y;
			this.add( settingsButton );
			
			removeCamera();
			
			return true;
		}
		
		return false
	}
	
	private function onSettingsClick() : void
	{
		Security.showSettings( SecurityPanel.PRIVACY );
		FP.stage.addEventListener( MouseEvent.MOUSE_OVER, onMouseOver );
	}
	
	private function onMouseOver( a_event : MouseEvent ) : void
	{
		var x : int = a_event.stageX;
		var y : int = a_event.stageY;
		if( x < 0 || x > FP.screen.width || y < 0 || y > FP.screen.height ) return;
			
		FP.stage.removeEventListener( MouseEvent.MOUSE_OVER, onMouseOver );
		
		tryAddingCamera();
	}
	
	private function onBackClick() : void
	{
		removeCamera();
		
		FP.world = new ChooseSourceWorld();
	}
	
	private function removeCamera() : void
	{
		if( _video != null )
		{
			if( _video.parent != null )
			{
				_video.parent.removeChild( _video );
			}
			
			_video.attachCamera( null );
		}
		
		_camera = null;
	}
	
	override public function update() : void
	{
		Input.mouseCursor = MouseCursor.AUTO;
		
		super.update();
	}
}

}