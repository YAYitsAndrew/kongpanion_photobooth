package
{

import net.flashpunk.graphics.Image;

public class RotateEntity extends OffsetDraggableEntity
{
	private var _img : Image;
	private var _sticker : StickerEntity;
	private var _startAngle : Number;
	
	public function RotateEntity( a_sticker : StickerEntity )
	{
		super();
		
		this.type = "interactive";
		
		_sticker = a_sticker;
		
		_img = new Image( Assets.ROTATE_HANDLE );
		this.setHitboxTo( _img );
		this.addGraphic( _img );
	}
	
	override public function onStartDrag() : void
	{
		super.onStartDrag();
		
		_sticker.draggable = false;
		
		_startAngle = _sticker.rotation + angleBetweenPoints( _sticker.x + _sticker.halfWidth,
			_sticker.y + _sticker.halfHeight, this.centerX, this.centerY );
	}
	
	override public function onEndDrag() : void
	{
		super.onEndDrag();
		
		_sticker.draggable = true;
	}
	
	override public function update() : void
	{
		super.update();
		
		if( ! this.isDragging )
		{
			this.x = _sticker.right;
			this.y = _sticker.top - this.height;
		}
		else
		{
			var newAngle : Number = angleBetweenPoints( _sticker.x + _sticker.halfWidth,
				_sticker.y + _sticker.halfHeight, this.centerX, this.centerY );
			_sticker.rotation = _startAngle - newAngle;
		}
	}
	
	private function angleBetweenPoints( a_p1x : Number, a_p1y : Number, a_p2x : Number,
		a_p2y : Number ) : Number
	{
		var xDiff : Number = a_p2x - a_p1x;
		var yDiff : Number = a_p2y - a_p1y;
		
		return Math.atan2( yDiff, xDiff );
	}
}

}