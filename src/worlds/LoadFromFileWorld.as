package worlds
{

import flash.display.BitmapData;
import flash.display.Loader;
import flash.display.LoaderInfo;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.net.FileFilter;
import flash.net.FileReference;

import net.flashpunk.FP;
import net.flashpunk.graphics.Image;
import net.flashpunk.graphics.Text;

import ui.ActiveButton;

public class LoadFromFileWorld extends LoadWorld
{
	private var _loadButton : ActiveButton;
	private var _fileRef : FileReference;
	private var _photoData : BitmapData;
	
	override public function begin() : void
	{
		super.begin();
		
		var msg : Text = new Text( "Load a file", 0, 0, { size : 16 } );
		msg.x = 8;
		msg.y = 16;
		
		var bg : Image = Image.createRect( FP.screen.width, msg.height + 32, 0x181818 );
		bg.alpha = 0.7;
		bg.y = msg.y - 16;
		this.addGraphic( bg );
		
		this.addGraphic( msg );
		
		_loadButton = new ActiveButton( 0, 0, "Choose...", 150, 50 );
		_loadButton.x = 8;
		_loadButton.y = bg.y + bg.height + 8;
		this.add( _loadButton );
		
		var backButton : ActiveButton = new ActiveButton( 0, 0, "Back", 150, 50, onBackClick );
		backButton.x = 8;
		backButton.y = FP.screen.height - backButton.height - 8;
		this.add( backButton );
		
		_totalHeight = _loadButton.y + _loadButton.height + 8;
		
		FP.stage.addEventListener( MouseEvent.CLICK, onMouseClick );
	}
	
	private function onMouseClick( a_event : MouseEvent ) : void
	{
		if( ! _loadButton.active ) return;
		
		if( _loadButton.collidePoint( _loadButton.x, _loadButton.y, a_event.stageX, a_event.stageY ) )
		{
			onLoadClick();
		}
	}
	
	private function onBackClick() : void
	{
		FP.stage.removeEventListener( MouseEvent.CLICK, onMouseClick );
		
		FP.world = new ChooseSourceWorld();
	}
	
	private function onLoadClick() : void
	{
		_loadButton.active = false;
		
		_fileRef = new FileReference();
		var imageFileTypes : FileFilter = new FileFilter( "Images (*.jpg, *.png)", "*.jpg;*.png" );
		
		_fileRef.browse( [ imageFileTypes ] );
		_fileRef.addEventListener( Event.SELECT, onFileRefSelect );
	}
	
	private function onFileRefSelect( a_event : Event ) : void
	{
		_fileRef.removeEventListener( Event.SELECT, onFileRefSelect );
		
		_fileRef.addEventListener( Event.COMPLETE, onLoadComplete );
		_fileRef.load();
	}
	
	private function onLoadComplete( a_event : Event ) : void
	{
		_fileRef.removeEventListener( Event.COMPLETE, onLoadComplete );
		
		var loader : Loader = new Loader();
		loader.contentLoaderInfo.addEventListener( Event.COMPLETE, onLoaderLoadComplete );
		loader.loadBytes( _fileRef.data );
	}
	
	private function onLoaderLoadComplete( a_event : Event ) : void
	{
		var loader : Loader = ( a_event.target as LoaderInfo ).loader;
		loader.contentLoaderInfo.removeEventListener( Event.COMPLETE, onLoaderLoadComplete );
		
		_photoData = new BitmapData( loader.width, loader.height );
		_photoData.draw( loader );
		
		addMessage( "Loaded: " + _fileRef.name );
		
		loadKongpanions();
	}
	
	override protected function onKongpanionLoadComplete() : void
	{
		FP.stage.removeEventListener( MouseEvent.CLICK, onMouseClick );
		
		FP.world = new PhotoboothWorld( _photoData );
	}
}

}