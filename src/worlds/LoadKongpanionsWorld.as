package worlds
{

import flash.events.Event;

import net.flashpunk.FP;
import net.flashpunk.World;
import net.flashpunk.graphics.Image;
import net.flashpunk.graphics.Text;

public class LoadKongpanionsWorld extends World
{
	override public function begin() : void
	{
		var msg : Text = new Text( "Loading Kongpanions...", 0, 0, { size : 24 } );
		msg.x = ( FP.screen.width - msg.width ) / 2;
		msg.y = ( FP.screen.height - msg.height ) / 2;
		
		var bg : Image = Image.createRect( FP.screen.width, msg.height + 32, 0x181818 );
		bg.alpha = 0.7;
		bg.y = msg.y - 16;
		this.addGraphic( bg );
		
		this.addGraphic( msg );
		
		var kj : KongpanionJson = new KongpanionJson();
		kj.addEventListener( Event.COMPLETE, onKongpanionJsonLoadComplete );
		kj.load();
	}
	
	private function onKongpanionJsonLoadComplete( a_event : Event ) : void
	{
		var kj : KongpanionJson = a_event.currentTarget as KongpanionJson;
		KongpanionPhotobooth.instance.kongpanionDatas = kj.kongpanionDatas;
		
		FP.world = new ChooseSourceWorld();
	}
}

}