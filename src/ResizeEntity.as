package
{

import flash.geom.Rectangle;

import net.flashpunk.graphics.Image;

public class ResizeEntity extends OffsetDraggableEntity
{
	private static var s_minScale : Number = 0.2;
	
	private var _img : Image;
	private var _sticker : StickerEntity;
	
	public function ResizeEntity( a_sticker : StickerEntity )
	{
		super();
		
		this.type = "interactive";
		
		_sticker = a_sticker;
		
		_img = new Image( Assets.RESIZE_HANDLE );
		this.setHitboxTo( _img );
		this.addGraphic( _img );
	}
	
	override public function onStartDrag() : void
	{
		super.onStartDrag();
		
		_sticker.draggable = false;
	}
	
	override public function onEndDrag() : void
	{
		super.onEndDrag();
		
		_sticker.draggable = true;
	}
	
	override public function update() : void
	{
		super.update();
		
		if( ! this.isDragging )
		{
			this.x = _sticker.x + _sticker.width;
			this.y = _sticker.y + _sticker.height;
		}
		else
		{
			var currRect : Rectangle = new Rectangle( _sticker.x, _sticker.y,
				_sticker.width / _sticker.scale, _sticker.height / _sticker.scale );
			var bigRect : Rectangle = new Rectangle( _sticker.x, _sticker.y,
				this.x - _sticker.x, this.y - _sticker.y );
			var newRect : Rectangle = fit( currRect, bigRect );
			
			var newScale : Number = newRect.width / currRect.width;
			if( newScale < s_minScale )
			{
				newScale = s_minScale;
			}
			_sticker.scale = newScale;
		}
	}
	
	private function fit(rectangle:Rectangle, into:Rectangle):Rectangle
	{
		var width:Number   = rectangle.width;
		var height:Number  = rectangle.height;
		var factorX:Number = into.width  / width;
		var factorY:Number = into.height / height;
		var factor:Number  = 1.0;
		var resultRect:Rectangle = new Rectangle();
		
		factor = factorX < factorY ? factorX : factorY;
		
		width  *= factor;
		height *= factor;
		
		resultRect.setTo(
			into.x + (into.width  - width)  / 2,
			into.y + (into.height - height) / 2,
			width, height);
		
		return resultRect;
	}
}

}