package
{

import net.flashpunk.Graphic;
import net.flashpunk.Mask;
import net.flashpunk.utils.Input;

public class OffsetDraggableEntity extends DraggableEntity
{
	protected var _startDragX : Number;
	protected var _startDragY : Number;
	
	public function OffsetDraggableEntity(x:Number=0, y:Number=0, graphic:Graphic=null, mask:Mask=null)
	{
		super(x, y, graphic, mask);
		
		_startDragX = 0;
		_startDragY = 0;
	}
	
	override public function onStartDrag() : void
	{
		super.onStartDrag();
		
		_startDragX = Input.mouseX - this.x;
		_startDragY = Input.mouseY - this.y;
	}
	
	override public function onEndDrag() : void
	{
		super.onEndDrag();
		
		_startDragX = 0;
		_startDragY = 0;
	}
	
	override public function update() : void
	{
		super.update();
		
		if( this.isDragging )
		{
			this.x -= _startDragX;
			this.y -= _startDragY;
		}
	}
}

}