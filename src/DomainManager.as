package
{

public class DomainManager
{
	// Credit: Adapted from Unknown Guardian, adapted from me, adapted from JackSmack.
	
	//////// Usage
	
	/*
	
	// Init with stage to get loaderURL
	DomainManager.init(stage);
	
	// Turns on local playback (on any machine), good for debugging
	DomainManager.setLocalOn();
	
	// Set which sites are allowed
	DomainManager.setAllowedSites("kongregate.com","yahoo.com","files.whatever.com");
	
	// Run to see if the current domain checks out
	trace( DomainManager.runCheck() ); // returns true or false
	
	// Check quickly if the domain is on Kong
	
	trace( DomainManager.isOnKong() ); // returns true or false
	
	*/
	
	private static var acceptedSites:Array = new Array();
	private static var protocol:String = "";
	private static var domain:String = "";
	
	private static var isLocal:Boolean = false;
	private static var isLocalAllowed:Boolean = false;
	
	public static function init(stage):void {
		var url:String = stage.loaderInfo.url;
		var urlStart:Number = url.indexOf("://") + 3;
		var urlEnd:Number = url.indexOf("/", urlStart);
		
		domain = url.substring(urlStart, urlEnd);
		var LastDot:Number = domain.lastIndexOf(".") - 1;
		var domEnd:Number = domain.lastIndexOf(".", LastDot) + 1;
		
		domain = domain.substring(domEnd, domain.length);
		protocol = url.substring(0, url.indexOf(":"));
	}
	
	public static function getDomain():String {
		return domain;
	}
	
	
	public static function setAllowedSites(sitesArray:Array):void {
		acceptedSites = sitesArray.concat();
		trace("DomainManager unlocked: " + acceptedSites);
	}
	
	public static function setLocalOn():void {
		isLocalAllowed = true;
		trace("DomainManager local unlocked");
	}
	
	public static function isOnKong():Boolean {
		
		switch(domain) {
			case "kongregate.com":
			case "kongshred.com":
			case "kongbus.com":
			case "kongcab.com":
			case "kongboat.com":
			case "kongzep.com":
			case "kongregatestage.com":
			case "kongregatetrunk.com":
				return true;
		}
		
		return false;
	}
	
	public static function isOnLocal():Boolean {
		return isLocal;
	}
	
	public static function runCheck():Boolean {
		if(domain == "" || domain == " ") {
			isLocal = true;
		}
		
		trace("Playing on \'"+domain+"\'");
		
		if ( acceptedSites.length == 0) {
			trace("No accepted sites passed.  Please use setAllowedSites() before starting. Not approving.");
			return false;
		} else if(acceptedSites.indexOf(domain) >= 0) {
			trace("Domain checks out.  Approving.");
			return true;
		} else if (isLocalAllowed && isLocal) {
			trace("Local is okay.  Approving.");
			return true;
		} else if(acceptedSites.indexOf(domain) == -1) {
			trace("No domain by that name found. Not approving.");
			return false;
		}
		
		trace("Could not find reason not to block.  Whatever.  Not approving.");
		return false;
	}
}

}