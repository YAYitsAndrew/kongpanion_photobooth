package worlds
{

import flash.events.Event;
import flash.net.URLRequest;
import flash.net.navigateToURL;
import flash.ui.MouseCursor;

import net.flashpunk.FP;
import net.flashpunk.graphics.Image;
import net.flashpunk.graphics.Text;
import net.flashpunk.utils.Input;

import ui.ActiveButton;

public class ChooseSourceWorld extends LoadWorld
{
	private var _bg : Image;
	private var _urlButton : ActiveButton;
	private var _cameraButton : ActiveButton;
	private var _fileButton : ActiveButton;
	
	override public function begin() : void
	{
		super.begin();
		
		var collage : Image = new Image( Assets.COLLAGE );
		this.addGraphic( collage );
		
		var msgText : String;
		
		if( DomainManager.isOnKong() && KongpanionPhotobooth.instance.webApi.services.isGuest() )
		{
			msgText = "Guests have no Kongpanions :(";
			
			addGuestButtons();
		}
		else if( countEarnedKongpanions() == 0 )
		{
			msgText = "Earn a Kongpanion first!";
			
			addEarnKongpanionButtons();
		}
		else
		{
			msgText = "Choose a photo";
			
			addPhotoButtons();
		}
		
		var msg : Text = new Text( msgText, 0, 0, { size : 24 } );
		msg.x = ( FP.screen.width - msg.width ) / 2;
		msg.y = 445 - msg.height;
		
		_bg = Image.createRect( FP.screen.width, msg.height + 32, 0x181818 );
		_bg.alpha = 0.7;
		_bg.y = msg.y - 16;
		this.addGraphic( _bg );
		
		this.addGraphic( msg );
	}
	
	private function addGuestButtons() : void
	{
		var msg : Text = new Text( "Register and start collecting Kongpanions by playing more games!" );
		msg.x = 8;
		msg.y = 477;
		this.addGraphic( msg );
		
		KongpanionPhotobooth.instance.webApi.services.addEventListener( "login", onKongregateInPageLogin );
		
		var signUpButton : ActiveButton = new ActiveButton( 0, 0, "Register", 150, 50, onSignUpClick );
		signUpButton.x = 8;
		signUpButton.y = msg.y + msg.height + 8;
		this.add( signUpButton );
	}
	
	private function onSignUpClick() : void
	{
		KongpanionPhotobooth.instance.webApi.services.showRegistrationBox();
	}
	
	private function onKongregateInPageLogin( a_event : Event ) : void
	{
		FP.world = new LoadKongpanionsWorld();
	}
	
	private function addEarnKongpanionButtons() : void
	{
		var msg : Text = new Text( "Collect Kongpanions by playing more games!" );
		msg.x = 8;
		msg.y = 477;
		this.addGraphic( msg );
		
		var earnButton : ActiveButton = new ActiveButton( 0, 0, "See today's Kongpanion", 250, 50, onEarnClick );
		earnButton.x = 8;
		earnButton.y = msg.y + msg.height + 8;
		this.add( earnButton );
	}
	
	private function onEarnClick() : void
	{
		var request : URLRequest = new URLRequest( "http://www.kongregate.com/kongpanions" );
		navigateToURL( request );
	}
	
	private function addPhotoButtons() : void
	{
		_urlButton = new ActiveButton( 0, 0, "Load from URL", FP.screen.width / 3 - 16,
			FP.screen.height / 5, onUrlClick );
		_urlButton.x = 8;
		_urlButton.y = FP.screen.height - _urlButton.height - 8;
		this.add( _urlButton );
		
		_cameraButton = new ActiveButton( 0, 0, "Take webcam photo", _urlButton.width,
			_urlButton.height, onCameraClick );
		_cameraButton.x = _urlButton.x + _urlButton.width + 16;
		_cameraButton.y = _urlButton.y;
		this.add( _cameraButton );
		
		_fileButton = new ActiveButton( 0, 0, "Open file", _urlButton.width, _urlButton.height,
			onFileClick );
		_fileButton.x = _cameraButton.x + _cameraButton.width + 16;
		_fileButton.y = _urlButton.y;
		this.add( _fileButton );
	}
	
	private function onUrlClick() : void
	{
		FP.world = new LoadFromUrlWorld();
	}
	
	private function onCameraClick() : void
	{
		FP.world = new LoadFromWebcamWorld();
	}
	
	private function onFileClick() : void
	{
		FP.world = new LoadFromFileWorld();
	}
	
	override public function update() : void
	{
		Input.mouseCursor = MouseCursor.AUTO;
		
		super.update();
	}
	
	private function countEarnedKongpanions() : int
	{
		var count : int = 0;
		
		for ( var i : int = 0; i < KongpanionPhotobooth.instance.kongpanionDatas.length; i++ )
		{
			var kd : KongpanionData = KongpanionPhotobooth.instance.kongpanionDatas[ i ];
			if( kd.isOwned )
			{
				count += 1;
			}
		}
		
		return count;
	}
}

}