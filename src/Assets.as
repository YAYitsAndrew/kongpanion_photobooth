package
{
import flash.display.BitmapData;
import flash.geom.Point;
import flash.geom.Rectangle;

public class Assets
{
	[Embed(source = "../assets/resize_handle.png")]
	public static const RESIZE_HANDLE : Class;
	
	[Embed(source = "../assets/rotate_handle.png")]
	public static const ROTATE_HANDLE : Class;
	
	[Embed(source = "../assets/mirror_handle.png")]
	public static const MIRROR_HANDLE : Class;
	
	[Embed(source = "../assets/collage.png")]
	public static const COLLAGE : Class;
	
	public static function trimWhitespace( a_bd : BitmapData ) : BitmapData
	{
		var topY : Number = 0;
		var botY : Number = a_bd.height;
		var leftX : Number = 0;
		var rightX : Number = a_bd.width;
		
		var x : int, y : int;
		var pixel : uint;
		
		outerTop: for( y = 0; y < a_bd.height; y++ )
		{
			for( x = 0; x < a_bd.width; x++ )
			{
				pixel = a_bd.getPixel32( x, y );
				if( ( ( pixel >> 24 ) & 0xFF ) > 0 )
				{
					topY = y;
					break outerTop;
				}
			}
		}
		
		outerBot: for( y = a_bd.height - 1; y >= 0; y-- )
		{
			for( x = 0; x < a_bd.width; x++ )
			{
				pixel = a_bd.getPixel32( x, y );
				if( ( ( pixel >> 24 ) & 0xFF ) > 0 )
				{
					botY = y;
					break outerBot;
				}
			}
		}
		
		outerLeft: for( x = 0; x < a_bd.width; x++ )
		{
			for( y = 0; y < a_bd.height; y++ )
			{
				pixel = a_bd.getPixel32( x, y );
				if( ( ( pixel >> 24 ) & 0xFF ) > 0 )
				{
					leftX = x;
					break outerLeft;
				}
			}
		}
		
		outerRight: for( x = a_bd.width - 1; x > 0; x-- )
		{
			for( y = 0; y < a_bd.height; y++ )
			{
				pixel = a_bd.getPixel32( x, y );
				if( ( ( pixel >> 24 ) & 0xFF ) > 0 )
				{
					rightX = x;
					break outerRight;
				}
			}
		}
		
		var bd : BitmapData = new BitmapData( rightX - leftX, botY - topY );
		bd.copyPixels( a_bd, new Rectangle( leftX, topY, bd.width, bd.height ), new Point() );
		
		return bd;
	}
}

}