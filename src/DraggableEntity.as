package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.utils.Input;
	
	/**
	 * ...
	 * @author azrafe7
	 */
	public class DraggableEntity extends Entity
	{
		public var draggable:Boolean = true;
		public var isDragging:Boolean = false;
		
		protected var _startDrag:Boolean = false;
		
		public function DraggableEntity(x:Number = 0, y:Number = 0, graphic:Graphic = null, mask:Mask = null)
		{
			super(x, y, graphic, mask);
		}
		
		override public function update():void 
		{
			super.update();
			
			if (Input.mousePressed && draggable && !isDragging && collidePoint(x, y, FP.world.mouseX, FP.world.mouseY)) {
				isDragging = true;
				onStartDrag();
			}
			
			if (Input.mouseReleased && draggable && isDragging) {
				isDragging = false;
				onEndDrag();
			}
			
			if (draggable && isDragging) {
				x = FP.world.mouseX;
				y = FP.world.mouseY;
			}
		}
		
		/**
		 * Override this, called when the Entity starts to being dragged.
		 */
		public function onStartDrag():void 
		{
			
		}

		/**
		 * Override this, called when the Entity stops to being dragged.
		 */
		public function onEndDrag():void 
		{
			
		}
	}

}