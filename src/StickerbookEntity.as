package
{

import com.greensock.loading.LoaderMax;
import com.greensock.loading.display.ContentDisplay;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.filters.ColorMatrixFilter;
import flash.geom.Point;
import flash.ui.MouseCursor;

import net.flashpunk.Entity;
import net.flashpunk.graphics.Image;
import net.flashpunk.utils.Input;
import worlds.PhotoboothWorld;

public class StickerbookEntity extends Entity
{
	private var _img : Image;
	private var _data : KongpanionData;
	private var _isShiny : Boolean;
	
	public function StickerbookEntity( a_data : KongpanionData, a_isShiny : Boolean )
	{
		super();
		
		_data = a_data;
		_isShiny = a_isShiny;
		
		var bmpName : String = _data.name;
		if( _isShiny )
		{
			bmpName += "_shiny";
		}
		
		var content : ContentDisplay = LoaderMax.getContent( bmpName );
		var bd : BitmapData = ( content.rawContent as Bitmap ).bitmapData;
		content = null;
		
		if( ! _data.isOwned || ( _isShiny && ! _data.isShiny ) )
		{
			var matrix : Array = [];
			matrix = matrix.concat( [ 0.3, 0.3, 0.3, 0, 0 ] );// red
			matrix = matrix.concat( [ 0.3, 0.3, 0.3, 0, 0 ] );// green
			matrix = matrix.concat( [ 0.3, 0.3, 0.3, 0, 0 ] );// blue
			matrix = matrix.concat( [ 0.0, 0.0, 0.0, 0.5, 0 ] );// alpha
			var filter : ColorMatrixFilter = new ColorMatrixFilter( matrix );
			bd.applyFilter( bd, bd.rect, new Point(), filter );
			
			this.active = false;
		}
		
		_img = new Image( Assets.trimWhitespace( bd ) );
		_img.smooth = true;
		_img.scale = 0.5;
		
		this.setHitbox( _img.scaledWidth, _img.scaledHeight );
		this.addGraphic( _img );
	}
	
	override public function update() : void
	{
		if( ! this.visible ) return;
		
		var mouseCollide : Boolean = this.collidePoint( x, y, Input.mouseX, Input.mouseY );
		
		if( mouseCollide )
		{	
			Input.mouseCursor = MouseCursor.BUTTON;
		}
		
		if( ! this.visible ) return;
		
		if( Input.mouseReleased && mouseCollide )
		{
			( this.world as PhotoboothWorld ).createSticker( _data, _isShiny );
		}
	}
}

}