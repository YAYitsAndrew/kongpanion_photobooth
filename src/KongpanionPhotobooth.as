package
{

import flash.display.Loader;
import flash.display.LoaderInfo;
import flash.display.StageScaleMode;
import flash.events.Event;
import flash.net.URLRequest;
import flash.system.Security;

import net.flashpunk.Engine;
import net.flashpunk.FP;

import worlds.LoadKongpanionsWorld;

[SWF( frameRate = "60", width = "800", height = "600", backgroundColor = "0xFFFFFF" )]
public class KongpanionPhotobooth extends Engine
{
	private static var s_instance : KongpanionPhotobooth;
	
	private var _webApi : *;
	private var _kongpanionDatas : Vector.<KongpanionData>;
	
	public function KongpanionPhotobooth()
	{
		super( 800, 600, 60, false );
		FP.screen.color = 0x9FB6CD;
		//FP.console.enable();
		
		s_instance = this;
	}
	
	override public function setStageProperties() : void
	{
		super.setStageProperties();
		stage.scaleMode = StageScaleMode.SHOW_ALL;
	}
	
	override public function init() : void
	{
		DomainManager.init( FP.stage );
		DomainManager.setLocalOn();
		DomainManager.runCheck();
		
		var paramObj : Object;
		if( this.root.loaderInfo != null )
		{
			paramObj = LoaderInfo( this.root.loaderInfo ).parameters;
		}
		else
		{
			paramObj = {};
		}
		
		var apiPath : String = paramObj.kongregate_api_path
			|| "http://www.kongregate.com/flash/API_AS3_Local.swf";
		Security.allowDomain( apiPath );
		
		var request : URLRequest = new URLRequest( apiPath );
		var loader : Loader = new Loader();
		loader.contentLoaderInfo.addEventListener( Event.COMPLETE, onKongApiLoaded );
		loader.load( request );
	}
	
	private function onKongApiLoaded( a_event : Event ) : void
	{
		_webApi = a_event.target.content;
		FP.stage.addChild( _webApi );
		_webApi.services.connect();
		
		FP.world = new LoadKongpanionsWorld();
	}
	
	public function get webApi() : * { return _webApi; }
	public function get kongpanionDatas() : Vector.<KongpanionData> { return _kongpanionDatas; }
	public function set kongpanionDatas( a_val : Vector.<KongpanionData> ) : void { _kongpanionDatas = a_val; }
	
	public static function get instance() : KongpanionPhotobooth { return s_instance; }
}

}